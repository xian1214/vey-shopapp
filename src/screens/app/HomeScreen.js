/* *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState, useContext } from 'react';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Button,
  FlatList,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';

import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage'
import Toast from 'react-native-simple-toast';
import SHOPAPIKit, { setShopClientToken } from '../../utils/apikit';
import { colors } from '../../res/style/colors'
import { Card } from 'react-native-paper';
const HomeScreen = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  const [liveOrders, setLiveOrders] = useState([]);
  const [selectedindex, setSelectedIndex] = useState(1)
  const data = [1, 2, 3, 4, 5];
  //const data = [1];
  
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      const bootstrapAsync = async () => {
        let userToken = null;
        try {
          userToken = await AsyncStorage.getItem('userToken')          
        } catch (e) {
          console.log(e);
        }
        // if (userToken != null) {
        //   const onSuccess = ({ data }) => {
        //     setLoading(false);
        //     getLiveOrders(data.liverOrders);
        //     console.log(data);
        //   }
        //   const onFailure = error => {
        //     console.log(error);
        //     setLoading(false);
        //   }
        //   setLoading(true);
        //   SHOPAPIKit.get('/user/allsubscription')
        //     .then(onSuccess)
        //     .catch(onFailure);
        // }
      };
      bootstrapAsync();
    });
    return unsubscribe;

  }, [navigation]);
  const getLiveOrders = (liveOrders) => {
    setLiveOrders(liveOrders);
  }
  const onOrderPressed = (item, index) => {
    console.log(index)
    setSelectedIndex(index)
  }
  const updateLiveOrders = () => {
    let id = liveOrders[selectedindex].subscriptionid
    console.log("subscription id: " + id);
    const payload = {usersubscriptionid: id };
    const onSuccess = ({ data }) => {
      setLoading(false);
      console.log(data);
      Toast.show('Successfully updated.');
      navigation.navigate('Home');
    }
    const onFailure = error => {
      console.log(error);
      setLoading(false);
      Toast.show('Failed to updat.');
    }
    setLoading(true);
    SHOPAPIKit.patch('/shop/update/liveorders', payload)
      .then(onSuccess)
      .catch(onFailure);
  }

  const renderItem = ({ item, index }) => {
    return (
      // <View style={selectedindex == index ? styles.selecteditem : styles.item}>
        <TouchableOpacity onPress={() => onOrderPressed(item, index)}>
        <View style={ styles.card }>
            <View style={styles.row}>
                <Text style={[styles.box, styles.box2]}> Order Ref</Text>
                <Text style={[styles.box, styles.box2]}>orderref</Text>
            </View>
            <View style={styles.row}>
                <Text style={[styles.box, styles.box2]}> Order value</Text>
                <Text style={[styles.box, styles.box3]}>₹ 29</Text>
            </View>
            <View style={styles.row}>
                <Text style={[styles.box, styles.box2]}> Total Quantity</Text>
                <Text style={[styles.box, styles.box3]}>3</Text>
            </View>
            <View style={styles.row}>
                <Text style={[styles.box, styles.box2]}> Order Pickup</Text>
                <Text style={[styles.box, styles.box3]}>10:00 AM</Text>
            </View>

        </View>
        </TouchableOpacity>
      // </View>
    )
  }
  return (
    <>
      <View style={styles.container}>
        <Spinner
          visible={loading} size="large" style={styles.spinnerStyle} />
        <Text style={styles.title}>Live Orders</Text>
        <FlatList
          data={data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={renderItem} />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(217,232,252,1)',
  },
  listContainer: {
    flex: 1,
  },
  title: {
    margin: 10,
    alignSelf: "center",
    fontSize: 16,
  },
  item: {
    margin: 10,
    flexDirection: 'row',
    backgroundColor: colors.white,
  },
  selecteditem: {
    margin: 10,
    flexDirection: 'row',
    backgroundColor: colors.white,
    borderWidth: 2,
    borderColor: '#00ff00',
  },
  card: {
    flexDirection: 'column',
    margin: 15,
    padding: 15,
    borderWidth: 2,
    borderColor: '#000000',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
  },

  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  box: {
    flex: 1,
    borderWidth: 1,
    borderColor: 'rgba(20,123,136,1)',
    height: 30,
    backgroundColor: '#333',
    textAlignVertical: "center",
    textAlign: "center",
  },
  box2: {
    backgroundColor: 'rgba(176, 227, 230, 1)',
  },
  box3: {
    // backgroundColor: 'orange'
    backgroundColor:  'rgba(178, 221, 238, 1)',

  },
  two: {
    flex: 2
  },
});

export default HomeScreen;