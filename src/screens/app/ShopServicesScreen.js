import React, { useEffect, useState, useContext } from 'react';
import { View, Text, TextInput, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
import Toast from 'react-native-simple-toast';
import Autocomplete from 'react-native-autocomplete-input';
// import SHOPAPIKit, { setShopClientToken } from '../../utils/apikit';
import Spinner from 'react-native-loading-spinner-overlay';
import {
    Input,
    Button
} from 'react-native-elements';

const ShopServicesScreen = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const [shopname, setShopName] = useState('');
    const [shoponwername, setShopOwnerName] = useState('');
    const [category, setCategory] = useState('');
    const [gst, setGST] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobileNumber] = useState('');
    const [password, setPassword] = useState('');
    const [address, setAddress] = useState('');
    const [landmark, setLandmark] = useState('');
    const [city, setCity] = useState('');
    const [query, setQuery] = useState('');
    const [cityData, setCityData] = useState([]);
    const [state, setState] = useState('');
    const [pincode, setPinCode] = useState('');

    const [errorData, setErrorData] = useState(
        {
            isValidShopName: true,
            isValidShopOwnerName: true,
            isValidCategory: true,
            isValidGST: true,
            isValidEmail: true,
            isValidMobile: true,
            isValidPassword: true,
            isValidAddress: true,            
            email: '',
            mobile: '',
            password: '',
            address: '',
            shopname: '',
            shoponwername: '',
            category: '',
            gst: ''
        });

    const handleUpdate = () => {
        // https://indicative.adonisjs.com
        const payload = { email, mobile, password, shopname, shoponwername, address, landmark, city, state, pincode };
        const onSuccess = ({ data }) => {
            setLoading(false);
            Toast.show('Successfully updated.');
        }
        const onFailure = error => {
            setLoading(false);
            console.log(error && error.response);
            Toast.show('Failed to update');
        }
        setLoading(true);
        SHOPAPIKit.patch('/user/update', payload)
            .then(onSuccess)
            .catch(onFailure)
    };

    useEffect(() => {
        const bootstrapAsync = async () => {
            let userToken = null;
            try {
                userToken = await AsyncStorage.getItem('userToken')
            } catch (e) {
                console.log(e);
            }
            // if (userToken != null) {
            //     const onSuccess = ({ data }) => {
            //         setLoading(false);
            //         setName(data.name);
            //         setEmail(data.email);
            //         setAddress(data.address);
            //         setLandmark(data.landmark);
            //         setState(data.state);
            //         setCity(data.city);
            //         setPinCode(data.pincode.toString());
            //     }
            //     const onFailure = error => {
            //         console.log(error);
            //         setLoading(false);
            //     }
            //     setLoading(true);
            //     SHOPAPIKit.get('/user/get')
            //         .then(onSuccess)
            //         .catch(onFailure);
            // }
            // else {

            // }
        };
        bootstrapAsync();

    }, []);
    const validateEmail = email => {
        var re = /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    
    const onShopNameChanged = () => {
        if (shopname.length < 3) {
            setErrorData({
                ...errorData,
                isValidShopName: true,
            });
            return false;
        }
        else {
            setErrorData({
                ...errorData,
                isValidShopName: false,
                shopname: 'Enter the valid shop name',
            });
            return false;
        }
    }
    const onShopOwnerNameChanged = () => {
        if (shoponwername.length < 3) {
            setErrorData({
                ...errorData,
                isValidShopOwnerName: true,
            });
            return false;
        }
        else {
            setErrorData({
                ...errorData,
                isValidShopOwnerName: false,
                shoponwername: 'Enter the valid shop owner name',
            });
            return false;
        }
    }
    const onCategoryChanged = () => {
        if (category.length < 3) {
            setErrorData({
                ...errorData,
                isValidCategory: true,
            });
            return false;
        }
        else {
            setErrorData({
                ...errorData,
                isValidCategory: false,
                category: 'Enter the valid category',
            });
            return false;
        }
    }
    const onGSTChanged = () => {
        if (gst.length < 3) {
            setErrorData({
                ...errorData,
                isValidGST: true,
            });
            return false;
        }
        else {
            setErrorData({
                ...errorData,
                isValidGST: false,
                gst: 'Enter the valid GST',
            });
            return false;
        }
    }
    const onMobileChanged = () => {
        if (mobile.length >= 8) {
            setErrorData({
                ...errorData,
                isValidMobile: true
            });
            const onSuccessMobile = ({ data }) => {
                setErrorData({
                    ...errorData,
                    isValidMobile: true,
                });
                setLoading(false);
            }
            const onFailureMobile = error => {
                setErrorData({
                    ...errorData,
                    isValidMobile: false,
                    mobile: 'Already existing mobile',
                });
                setLoading(false);
            }
            setLoading(true);
            SHOPAPIKit.get('/validation/mobile/' + mobile)
                .then(onSuccessMobile)
                .catch(onFailureMobile);
        }
        else {
            setErrorData({
                ...errorData,
                isValidMobile: false,
                mobile: 'Enter the valid phone number',
            });
        }
    }    
    const checkMobileNumber = () => {
        if (mobile.length >= 8) {
            setErrorData({
                ...errorData,
                isValidMobile: true
            });
            return true;
        }
        else {
            setErrorData({
                ...errorData,
                isValidMobile: false,
                mobile: 'Enter the valid phone number',
            });
            return false;
        }
    }
    const checkEmail = () => {
        if (validateEmail(email)) {
            setErrorData({
                ...errorData,
                isValidEmail: true
            });
            return true;
        }
        else {
            setErrorData({
                ...errorData,
                isValidEmail: false,
                email: 'Enter the valid email',
            });
            return false;
        }
    }
    const onEmailChanged = () => {
        if (validateEmail(email)) {
            setErrorData({
                ...errorData,
                isValidEmail: true
            });
            const onSuccessEmail = ({ data }) => {
                setErrorData({
                    ...errorData,
                    isValidEmail: false,
                    email: 'Already existing email',
                });
                setLoading(false);
            }
            const onFailureEmail = error => {
                setErrorData({
                    ...errorData,
                    isValidEmail: true,
                });
                setLoading(false);
            }
            setLoading(true);
            SHOPAPIKit.get('/validation/email/' + email)
                .then(onSuccessEmail)
                .catch(onFailureEmail);
        }
        else {
            if(email.length > 0){
                setErrorData({
                    ...errorData,
                    isValidEmail: false,
                    email: 'Enter the valid email',
                });    
            }
            else{
                setErrorData({
                    ...errorData,
                    isValidEmail: true,
                });
            }
        }
    }
    const onPasswordChanged = () => {
        if (password.length < 5) {
            setErrorData({
                ...errorData,
                isValidPassword: false,
                password: 'Password length should be greater than 5',
            });
            return false;
        }
        else {
            setErrorData({
                ...errorData,
                isValidPassword: true
            });
            return true;
        }
    }
    const onAddressChanged = () => {
        if (address.length < 3) {
            setErrorData({
                ...errorData,
                isValidAddress: false,
                address: 'Enter the valid address',
            });
            console.log(errorData);
            return false;
        }
        else {
            setErrorData({
                ...errorData,
                isValidAddress: true
            });
            return true;
        }
    }
    const onChangeCity = (city) => {
        console.log(city);
        setCity(city);
        const onSuccessEmail = ({ data }) => {
            setCityData(data.usercities);
        }
        const onFailureEmail = error => {
            setCityData([]);
        }
        SHOPAPIKit.get('/validation/city/' + city)
            .then(onSuccessEmail)
            .catch(onFailureEmail);
    }
    const onSelectedCity = (item) => {
        setCity(item.city);
        setQuery(item.city);
        setState(item.state);
        setPinCode(item.pincode.toString());
        setCityData([]);
        
    }

    return (
        <View style={styles.container}>
            <Spinner
                visible={loading} size="large" style={styles.spinnerStyle} />
            <ScrollView
                style={styles.scrollView}>
                    <View style={styles.viewInputGroup}>
                        <View>
                            <Text style={styles.textView}>
                                GST
                            </Text>
                            <TextInput
                                style={styles.textInput}
                                label={'GST'}
                                placeholder="GST"
                                value={gst}
                                onChangeText={setGST}
                                onBlur={() => onGSTChanged()}
                            />
                            {
                                errorData.isValidGST ? null : <Text style={{ color: 'red' }}>{errorData.gst}</Text>
                            }
                        </View>
                        <View>
                            <Text style={styles.textView}>
                                Incorporation year
                            </Text>
                            <TextInput
                                style={styles.textInput}
                                label={'year'}
                                placeholder="Incorporation year"
                                // onBlur={() => onShopOwnerNameChanged()}
                            />
                        </View>
                        
                        <Button
                            buttonStyle={styles.updateButton}
                            backgroundColor="#03A9F4"
                            title="Update shop details"
                            onPress={() => handleUpdate()}
                        />
                </View>
            </ScrollView>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
    },
    spinnerStyle: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
    },
    scrollView: {
        marginTop: 50,
    },
    
    loginButton: {
        margin: 10,
        marginTop: 30,
    },
    updateButton: {
        marginTop: 30,
        marginBottom: 80,
    },
    viewInputGroup: {
        borderRadius: 25,
        marginBottom: 30,
        justifyContent: "center",
        paddingLeft: 30,
        paddingRight: 30,
    },
    viewTitleGroup: {
        fontSize: 16,
        marginTop: 20,
        color: "rgba(64,64,64,1)",
        fontWeight: 'bold',
    },
    textView: {
        fontSize: 14,
        marginTop: 10,
        color: "rgba(64,64,64,1)",
    },
    textInput: {
        borderWidth: 1,
        borderColor: 'gray',
        paddingLeft: 8,
        height: 40,
        textAlignVertical: "center"
    },
})
export default ShopServicesScreen;