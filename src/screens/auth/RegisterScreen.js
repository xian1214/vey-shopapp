import React, { useEffect, useState, useContext } from 'react';
import { View, Text, TextInput, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import Toast from 'react-native-simple-toast';
import Autocomplete from 'react-native-autocomplete-input';
import SHOPAPIKit, { setShopClientToken } from '../../utils/apikit';
import Spinner from 'react-native-loading-spinner-overlay';
import Geolocation from '@react-native-community/geolocation';
import DropDownPicker from 'react-native-dropdown-picker';

import {
    Button
} from 'react-native-elements';

import { AuthContext } from '../../utils/authContext';
import { reducer } from '../../reducer'

const RegisterScreen = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const [shopname, setShopName] = useState('');
    const [shopowner, setShopOwner] = useState('');
    const [shopcategory, setCategory] = useState('');
    const [shopcategories, setCategories] = useState([]);
    const [gst, setGST] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobileNumber] = useState('');
    const [password, setPassword] = useState('');
    const [address, setAddress] = useState('');
    const [landmark, setLandmark] = useState('');
    const [city, setCity] = useState('');
    const [query, setQuery] = useState('');
    const [cityData, setCityData] = useState([]);
    const [state, setState] = useState('');
    const [pincode, setPinCode] = useState('');
    const [latitude, setLatitude] = useState('18.571391');
    const [longitude, setLongitude] = useState('73.774795');

    const [errorData, setErrorData] = useState(
        {
            isValidShopName: true,
            isValidShopOwner: true,
            isValidCategory: true,
            isValidGST: true,
            isValidEmail: true,
            isValidMobile: true,
            isValidPassword: true,
            isValidAddress: true,            
            email: '',
            mobile: '',
            password: '',
            address: '',
            shopname: '',
            shopowner: '',
            shopcategory: '',
            gst: ''
        });

    const { signIn, signOut } = useContext(AuthContext); // should be signUp

    const handleSignUp = () => {
        // https://indicative.adonisjs.com

        if(onShopNameChanged() == false)
        {
            Toast.show('Enter the show name')
            return;
        }
        if(onShopOwnerNameChanged() == false)
        {
            Toast.show('Enter the shop owner name')
            return;
        }
        if(onAddressChanged() == false)
        {
            Toast.show('Enter the address')
            return;
        }
        if(onCategoryChanged() == false)
        {
            Toast.show('Enter the category')
            return;
        }
        if(onLandMarkChanged() == false)
        {
            Toast.show('Enter the landmark')
            return;
        }
        if(onGSTChanged() == false)
        {
            Toast.show('Enter the valid GST')
            return;
        }
        if(onEmailChanged() == false)
        {
            Toast.show('Enter the valid email')
            return;
        }
        if(onMobileChanged() == false)
        {
            Toast.show('Enter the mobile')
            return;
        }
        if(onPasswordChanged() == false)
        {
            Toast.show('Enther the password')
            return;
        }

        if(checkCity() == false)
        {
            Toast.show('Enter the city')
            return;
        }
        if(checkState() == false)
        {
            Toast.show('Enter the State')
            return;
        }
        if(checkPincode() == false)
        {
            Toast.show('Enter the pincode')
            return;
        }

        console.log('success sign in');
        const payload = {gst, shopname, shopowner, email, mobile, password, address, landmark, city, state, pincode, shopcategory, latitude, longitude};

        const onSuccess = ({ data }) => {
            console.log(data);
            setLoading(false);
            if(data.hasError == false){                
                Toast.show('Successfully registered');
                signOut();
            }
            // else
            //     Toast.show(data.error.message);
        }
        const onFailure = error => {
            setLoading(false);
            console.log(error && error.response);
            Toast.show('Failed to register');
        }
        setLoading(true);
        SHOPAPIKit.post('/shop/register', payload)
            .then(onSuccess)
            .catch(onFailure);
    };

    const setCategoryData = (items) => {
         var data = [];
         items.forEach(item => {
            data.push({label: item.shopcategory, value: item.shopcategoryid});
         });
         setCategories(data);

         // items={[
         //     {label: 'UK', value: 'uk'},
         //     {label: 'France', value: 'france'},
         // ]}
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            Geolocation.getCurrentPosition(info =>
              {
                if(info.coords != undefined){
                    setLatitude(info.coords.latitude)                
                    setLongitude(info.coords.longitude)
                }
              }
            );

            const onSuccess = ({ data }) => {
                setLoading(false);
                setCategoryData(data.shopcategories)
                console.log(data.shopcategories)
            }
            const onFailure = error => {
                setLoading(false);
                setCategoryData([]);
            }
            setLoading(true);
            SHOPAPIKit.get('/shop/allcategory/')
                .then(onSuccess)
                .catch(onFailure);
          });
          return unsubscribe;
     }, 
    [errorData]);

    const validateEmail = email => {
        var re = /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    
    const checkCity = () => {
        if (city.length > 1) {
            return true;
        }
        else {
            return false;
        }
    }
    const checkState = () => {
        if (state.length > 1) {
            return true;
        }
        else {
            return false;
        }
    }
    const checkPincode = () => {
        if (pincode.length > 1) {
            return true;
        }
        else {
            return false;
        }
    }
    const onShopNameChanged = () => {
        if (shopname.length > 3) {
            setErrorData({
                ...errorData,
                isValidShopName: true,
            });
            return true;
        }
        else {
            setErrorData({
                ...errorData,
                isValidShopName: false,
                shopname: 'Enter the valid shop name',
            });
            return false;
        }
    }
    const onShopOwnerNameChanged = () => {
        if (shopowner.length > 3) {
            setErrorData({
                ...errorData,
                isValidShopOwner: true,
            });
            return true;
        }
        else {
            setErrorData({
                ...errorData,
                isValidShopOwner: false,
                shopowner: 'Enter the valid shop owner name',
            });
            return false;
        }
    }
    const onCategoryChanged = () => {
        console.log(shopcategory);
        if (shopcategory.toString().length > 0) {
            setErrorData({
                ...errorData,
                isValidCategory: true,
            });
            return true;
        }
        else {
            setErrorData({
                ...errorData,
                isValidCategory: false,
                shopcategory: 'Enter the valid category',
            });
            return false;
        }
    }
    const onGSTChanged = () => {
        if (gst.length == 15) {
            setErrorData({
                ...errorData,
                isValidGST: true
            });
            const onSuccess = ({ data }) => {
                setErrorData({
                    ...errorData,
                    isValidGST: true,
                });
                setLoading(false);
                return true;
            }
            const onFailure = error => {
                setErrorData({
                    ...errorData,
                    isValidGST: false,
                    mobile: 'Invalid GST',
                });
                setLoading(false);
                return false;
            }
            setLoading(true);
            SHOPAPIKit.get('/validation/gst/' + gst)
                .then(onSuccess)
                .catch(onFailure);
        }
        else {
            setErrorData({
                ...errorData,
                isValidGST: false,
                mobile: 'Enter the GST that character length is 15.',
            });
            return false;
        }
    }
    const onEmailChanged = () => {
        if (validateEmail(email)) {
            setErrorData({
                ...errorData,
                isValidEmail: true
            });
            const onSuccessEmail = ({ data }) => {
                setErrorData({
                    ...errorData,
                    isValidEmail: true,
                });
                setLoading(false);
                return true;
            }
            const onFailureEmail = error => {
                setErrorData({
                    ...errorData,
                    isValidEmail: false,
                    email: 'Already existing email',
                });
                setLoading(false);
                return false;
            }
            setLoading(true);
            SHOPAPIKit.get('/validation/email/' + email)
                .then(onSuccessEmail)
                .catch(onFailureEmail);
        }
        else {
            if(email.length > 0){
                setErrorData({
                    ...errorData,
                    isValidEmail: false,
                    email: 'Enter the valid email',
                }); 
                return false;
            }
            else{
                setErrorData({
                    ...errorData,
                    isValidEmail: true,
                });
                return true;
            }
        }
    }
    const onMobileChanged = () => {
        if (mobile.length >= 8) {
            setErrorData({
                ...errorData,
                isValidMobile: true
            });
            const onSuccessMobile = ({ data }) => {
                setErrorData({
                    ...errorData,
                    isValidMobile: true,
                });
                setLoading(false);
                return true;
            }
            const onFailureMobile = error => {
                setErrorData({
                    ...errorData,
                    isValidMobile: false,
                    mobile: 'Already existing mobile',
                });
                setLoading(false);
                return false;
            }
            setLoading(true);
            SHOPAPIKit.get('/validation/mobile/' + mobile)
                .then(onSuccessMobile)
                .catch(onFailureMobile);
        }
        else {
            setErrorData({
                ...errorData,
                isValidMobile: false,
                mobile: 'Enter the valid phone number',
            });
            return false;
        }
    }
    const onPasswordChanged = () => {
        if (password.length < 5) {
            setErrorData({
                ...errorData,
                isValidPassword: false,
                password: 'Password length should be greater than 5',
            });
            return false;
        }
        else {
            setErrorData({
                ...errorData,
                isValidPassword: true
            });
            return true;
        }
    }
    const onAddressChanged = () => {
        if (address.length < 3) {
            setErrorData({
                ...errorData,
                isValidAddress: false,
                address: 'Enter the valid address',
            });
            console.log(errorData);
            return false;
        }
        else {
            setErrorData({
                ...errorData,
                isValidAddress: true
            });
            return true;
        }
    }
    const onLandMarkChanged = () => {
        if (landmark.length < 3) {
            setErrorData({
                ...errorData,
                isValidAddress: false,
                address: 'Enter the valid landmark',
            });
            console.log(errorData);
            return false;
        }
        else {
            setErrorData({
                ...errorData,
                isValidAddress: true
            });
            return true;
        }
    }
    const onChangeCity = (city) => {
        console.log(city);
        setCity(city);
        const onSuccessEmail = ({ data }) => {
            setCityData(data.shopcities);
        }
        const onFailureEmail = error => {
            setCityData([]);
        }
        SHOPAPIKit.get('/validation/city/' + city)
            .then(onSuccessEmail)
            .catch(onFailureEmail);
    }
    const onSelectedCity = (item) => {
        setCity(item.city);
        setQuery(item.city);
        setState(item.state);
        setPinCode(item.pincode.toString());
        setCityData([]);
    }

    return (
        <View style={styles.container}>
            <Spinner
                visible={loading} size="large" style={styles.spinnerStyle} />
            <ScrollView
                style={styles.scrollView}>
                <View style={styles.viewInputGroup}>
                    <View>
                        <Text style={styles.viewTitleGroup}>
                            Shop
                        </Text>
                        <View>
                            <Text style={styles.textView}>
                                Name
                            </Text>
                            <TextInput
                                style={styles.textInput}
                                label={'ShopName'}
                                placeholder="Shop Name"
                                value={shopname}
                                onChangeText={setShopName}
                                onBlur={() => onShopNameChanged()}
                            />
                            {
                                errorData.isValidShopName ? null : <Text style={{ color: 'red' }}>{errorData.shopname}</Text>
                            }
                        </View>
                        <View>
                            <Text style={styles.textView}>
                                Owner Name
                            </Text>
                            <TextInput
                                style={styles.textInput}
                                label={'ShopOwnerName'}
                                placeholder="Shop Owner Name"
                                value={shopowner}
                                onChangeText={setShopOwner}
                                onBlur={() => onShopOwnerNameChanged()}
                            />
                            {
                                errorData.isValidShopOwner ? null : <Text style={{ color: 'red' }}>{errorData.shopowner}</Text>
                            }
                        </View>
                        <View>
                            <Text style={styles.textView}>
                                Category
                            </Text>
                            <DropDownPicker
                            /*
                                items={[
                                    {label: 'UK', value: 'uk'},
                                    {label: 'France', value: 'france'},
                                ]}
                            */    
                                items={shopcategories}
                                //defaultValue={'uk'}
                                containerStyle={{height: 40}}
                                style={{backgroundColor: '#fafafa'}}
                                dropDownStyle={{backgroundColor: '#fafafa'}}
                                onChangeItem={item => setCategory(item.value)}

                            />
                            {/* {
                                errorData.isValidCategory ? null : <Text style={{ color: 'red' }}>{errorData.shopcategory}</Text>
                            } */}
                        </View>
                        <View>
                            <Text style={styles.textView}>
                                GST
                            </Text>
                            <TextInput
                                style={styles.textInput}
                                label={'GST'}
                                placeholder="GST"
                                value={gst}
                                onChangeText={setGST}
                                onBlur={() => onGSTChanged()}
                            />
                            {
                                errorData.isValidGST ? null : <Text style={{ color: 'red' }}>{errorData.gst}</Text>
                            }
                        </View>
                    </View>
                    <View>
                        <Text style={styles.viewTitleGroup}>
                            Contact
                        </Text>
                        <View>
                            <Text style={styles.textView}>
                                Email
                            </Text>
                            <TextInput
                                style={styles.textInput}
                                label={'Email'}
                                placeholder="Email Address"
                                value={email}
                                onChangeText={setEmail}
                                onBlur={() => onEmailChanged()}
                            />
                            {
                                errorData.isValidEmail ? null : <Text style={{ color: 'red' }}>{errorData.email}</Text>
                            }
                        </View>
                        <View>
                            <Text style={styles.textView}>
                                Mobile
                            </Text>
                            <TextInput
                                style={styles.textInput}
                                label={'Mobile'}
                                placeholder="Mobile Number"
                                value={mobile}
                                onChangeText={setMobileNumber}
                                onBlur={() => onMobileChanged()}
                            />
                            {
                                errorData.isValidMobile ? null : <Text style={{ color: 'red' }}>{errorData.mobile}</Text>
                            }
                        </View>
                        <View>
                            <Text style={styles.textView}>
                                Password
                            </Text>
                            <TextInput
                                style={styles.textInput}
                                label={'Password'}
                                placeholder="Password"
                                value={password}
                                onChangeText={setPassword}
                                secureTextEntry
                                onBlur={() => onPasswordChanged()}
                            />
                            {
                                errorData.isValidPassword ? null : <Text style={{ color: 'red' }}>{errorData.password}</Text>
                            }
                        </View>                        
                    </View>

                    <View>
                        <Text  style={styles.viewTitleGroup}>
                            Address
                        </Text>
                        <View>
                        <Text style={styles.textView}>
                                Address
                            </Text>
                            <TextInput
                                style={styles.textInput}
                                label={'Address'}
                                placeholder="Address"
                                value={address}
                                onChangeText={setAddress}
                                onBlur={() => onAddressChanged()}
                            />
                            {
                                errorData.isValidAddress ? null : <Text style={{ color: 'red' }}>{errorData.address}</Text>
                            }
                        </View>
                        <View>
                        <Text style={styles.textView}>
                                Landmark
                            </Text>
                            <TextInput
                                style={styles.textInput}
                                label={'Landmark'}
                                placeholder="Landmark"
                                value={landmark}
                                onChangeText={setLandmark}
                                onBlur={() => onLandMarkChanged()}
                            />
                        </View>
                        <View>
                            <Text style={styles.textView}>
                                City
                            </Text>
                            <Autocomplete
                                style={styles.textInput}
                                label={'City'}
                                placeholder="City"
                                data={cityData}
                                defaultValue={query}
                                value={city}
                                onChangeText={text=> onChangeCity(text)}
                                renderItem={({ item, i }) => (
                                    <TouchableOpacity onPress={() => onSelectedCity(item)}>
                                    <Text>{item.city}</Text>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>
                        <View>
                            <Text style={styles.textView}>
                                State
                            </Text>
                            <TextInput
                                style={styles.textInput}
                                label={'State'}
                                placeholder="State"
                                value={state}
                                onChangeText={setState}
                            />
                        </View>
                        <View>
                            <Text style={styles.textView}>
                                Pincode
                            </Text>
                            <TextInput
                                style={styles.textInput}
                                label={'Pincode'}
                                placeholder="Pincode"
                                value={pincode}
                                onChangeText={setPinCode}
                            />
                        </View>
                    </View>
                    <Button
                        buttonStyle={styles.registerButton}
                        backgroundColor="#03A9F4"
                        title="Register"
                        onPress={() => handleSignUp()}
                    />
                    <View style={{flexDirection: 'row', alignSelf: "center", marginBottom: 80}}>
                        <Text style = {{textAlign: "center", color: "rgba(64,64,64,1)"}}>Already Registered? </Text>
                        <TouchableOpacity 
                            onPress={() => signIn()}>
                            <Text
                                style={styles.underLineText}>
                                Login
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
    },
    spinnerStyle: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
    },
    scrollView: {
        marginTop: 50,
    },
    
    loginButton: {
        margin: 10,
        marginTop: 30,
    },
    registerButton: {
        marginTop: 30,
        marginBottom: 10,
    },
    viewInputGroup: {
        borderRadius: 25,
        marginBottom: 30,
        justifyContent: "center",
        paddingLeft: 30,
        paddingRight: 30,
    },
    viewTitleGroup: {
        fontSize: 16,
        marginTop: 20,
        color: "rgba(64,64,64,1)",
        fontWeight: 'bold',
    },
    textView: {
        fontSize: 14,
        marginTop: 10,
        color: "rgba(64,64,64,1)",
    },
    textInput: {
        borderWidth: 1,
        borderColor: 'gray',
        paddingLeft: 8,
        height: 40,
        textAlignVertical: "center"
    },
    loginText: {
        fontSize: 16,
        textDecorationLine: 'underline',
        color: "rgba(0,255,0,1)",
        textAlign: "center"
    },
    underLineText: {
        fontSize: 14,
        textDecorationLine: 'underline',
        color: "rgba(34,137,220,1)",
        // fontWeight: 'bold',
        textAlign: 'center',
    }
})
export default RegisterScreen;